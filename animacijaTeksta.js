$('.tlt').textillate({
    // the default selector to use when detecting multiple texts to animate
    selector: '.texts',

    // enable looping
    loop: true,

    // sets the minimum display time for each text before it is replaced
    minDisplayTime: 1000,

    // sets the initial delay before starting the animation
    // (note that depending on the in effect you may need to manually apply
    // visibility: hidden to the element before running this plugin)
    initialDelay: 50,

    // set whether or not to automatically start animating
    autoStart: false,

    // in animation settings
    in: {
        // set the effect name
        effect: 'flash',

        // set the delay factor applied to each consecutive character
        delayScale: 1.5,

        // set the delay between each character
        delay: 50,

        // set to true to animate all the characters at the same time
        sync: false,

        // randomize the character sequence
        // (note that shuffle doesn't make sense with sync = true)
        shuffle: true,

        // reverse the character sequence
        // (note that reverse doesn't make sense with sync = true)
        reverse: false,

        // callback that executes once the animation has finished
        callback: function () {
            $('.top_left').addClass('rotateInLeft').show();
            $('.bottom_left').addClass('rotateInLeft').show();
            $('.top_right').addClass('rotateInRight').show();
            $('.bottom_right').addClass('rotateInRight').show();
            /* $(".top_left").one(animationEvent, function(event) {
                $('.top_right').addClass('rotateInRight').show();
                $('.bottom_right').addClass('rotateInRight').show();
            }); */
            
            $(".bottom_right").one(animationEvent, function(event) {
                $('.arrow_container img').addClass('floater').removeClass('d-none');;
            });
         }
    },
    out: {
        effect: 'zoomOut',
        delayScale: 1.5,
        delay: 50,
        sync: false,
        shuffle: true,
        reverse: false,
        callback: function () {}
    },
    // set the type of token to animate (available types: 'char' and 'word')
    type: 'char'
});



var animations = {
    animation: 'animationend',
    OAnimation: 'oAnimationEnd',
    MozAnimation: 'mozAnimationEnd',
    WebkitAnimation: 'webkitAnimationEnd',
  };

  function whichAnimationEvent(){
    var t, el = document.createElement("fakeelement");    
    for (t in animations){
      if (el.style[t] !== undefined){
        return animations[t];
      }
    }
  }
  
  var animationEvent = whichAnimationEvent();
  
  
$(".topL h2").one(animationEvent, function(event) {
    $(".topR h2").addClass('slideDown').removeClass('d-none');            
});
$(".topR h2").one(animationEvent, function(event) {
    $(".bottomR h2").addClass('slideLeft').removeClass('d-none');
});
$(".bottomR h2").one(animationEvent, function(event) {
    $(".bottomL h2").addClass('slideUp').removeClass('d-none');
});

$(".bottomL h2").one(animationEvent, function(event) {
    $('.tlt').on('inAnimationBegin.tlt', function () {
        $('.tlt').addClass("zoomer").show();
    });
    $('.tlt').textillate('start');
});
     
/****************************************************
 *****************************************************
 * *************      sec 2       ***********************
 * *****************************************************
 ******************************************************/
