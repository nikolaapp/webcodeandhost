$(window).on('resize', function () {
    $('section').css("height", window.innerHeight);
    animScroll($('section.active'));
});

$('section').css("height", window.innerHeight);

$("nav a").click(function () {
    $('nav a').removeClass("active");
    $(this).addClass("active");

    $('section').removeClass("active");
    let href = $(this).attr("href");
    $(href).addClass("active");

    animScroll(href);
});

$("div.hamburger").click(function () {
    $(this).toggleClass("is-active");
    $('menu').slideToggle('fast', 'swing', function () { });
    $('.hamburger').toggleClass('rounded border border-primary');
});

/* The flag that determines whether the wheel event is supported. */
var supportsWheel = false;
/* The function that will run when the events are triggered. */
function func_scrollEvent(event) {
    event.preventDefault();
    /* Check whether the wheel event is supported. */
    if (event.type == "wheel") supportsWheel = true;
    else if (supportsWheel) return;

    /* Determine the direction of the scroll (< 0 → up, > 0 → down). */
    var delta = ((event.deltaY || -event.wheelDelta || event.detail) >> 10) || 1;
    if (delta > 0) {
        section_up_down('down');
    } else {
        section_up_down('up');
    }
}

/* Add the event listeners for each event. */
document.addEventListener('wheel', func_scrollEvent);
document.addEventListener('mousewheel', func_scrollEvent);
document.addEventListener('DOMMouseScroll', func_scrollEvent);
document.onkeydown = function (event) {
    switch (event.keyCode) {
        case 33: //page up
        case 38: //up arrow
            section_up_down('up');
            break;
        case 34: //page down
        case 40: //down arrow
            section_up_down('down');
            break;
        case 37://alert('left');
            break;
        case 39://alert('right');
            break;
    }
};

var multipleEvents = false; // flag za kontrolisanje uzastopnih eventa
/**
 * Handler prelaska sa jedne sekcija na drugu
 * @param {String} selector 'up'/'down'
 */
function section_up_down(selector) {
    if (multipleEvents) return; // ne dozvoljava uzastopne obrade vise eventa
    multipleEvents = true;
    setTimeout(function () {
        multipleEvents = false
    }, 500);

    let $sectionActive_now = $('section.active');
    let sectionActive_tobecome;
    if (selector == 'up') {
        sectionActive_tobecome = $sectionActive_now.prev().prev()[0];
    } else if (selector == 'down') {
        sectionActive_tobecome = $sectionActive_now.next().next()[0];
    }
    else { return; }

    if (sectionActive_tobecome) {
        $sectionActive_now.removeClass("active");
        $(sectionActive_tobecome).addClass("active");

        let $aActive = $('nav a.active');
        $aActive.removeClass("active");
        if (selector == 'up') {
            $aActive.parent().prev('li').find('a').addClass("active");
        } else if (selector == 'down') {
            $aActive.parent().next('li').find('a').addClass("active");
        }

        animScroll(sectionActive_tobecome);
    }
}

/**
 * Animacija prelaska sa jedne na drugu sekciju
 * @param {Element} element do kojeg radimo skrolovanje
 */
function animScroll(element) {
    $('html, body').animate(
        {
            scrollTop: $(element).offset().top
        },
        1500,
        "swing",
        function () {
            if ($("#sec2").hasClass('active')) {
                animirajSec2();
                // na svakih 5sec ponovi animaciju
                setInterval(function () {
                    animirajSec2();
                }, 5000);
                
            }
        }
    );
}

/**
 * funkcija koja animira pozadinu sekcije2
 * - translira slike koje se nalaze u pozadini
 */
function animirajSec2() {
    let nizSlobodnihPoz = createGridSpaceForImg();
    let pozicija, index;
    $('#sec2 .imgs_container img').each(function () {
        index = randBroj(0, nizSlobodnihPoz.length - 1);
        pozicija = nizSlobodnihPoz.splice(index, 1);
        console.log(nizSlobodnihPoz.length);
        $(this).css("top", pozicija[0][0] + "%");
        /* $(this).css("bottom",pozicija[0][1]+"%"); */
        $(this).css("left", pozicija[0][2] + "%");
        /*  $(this).css("right",pozicija[0][3]+"%"); */
    });
}

/**
 * Vraca random broj u rasponu od - do
 * @param {Number} from od
 * @param {Number} to do
 * @returns {Number} random broj
 */
function randBroj(from, to) {
    let rand = Math.floor(Math.random() * (parseInt(to) - parseInt(from))) + parseInt(from);
    /* let outVal = rand + "%";
    console.log(outVal); */
    return rand;
}
/**
 * vraca niz pozicija na koje mogu da se smeste slike za pozadinu
 * @return {Array[]} [ [10,20,30,40],  [10,30,40,40] ... ]
 */
function createGridSpaceForImg() {
    let nizPozicija = [];
    let min = 10;
    let max = 70;
    let increment = 15;
    let epsilon = 3; // varijacija vrednosti - cool prikaz da nebi bio striktno grid
    let bottom = 0, right = 0;
    // ne vrsi mi nikakvu ulogu bottom i right - dovoljno mi je top i left
    for (let top = min; top <= max; top += increment)
        /* for (let bottom = min; bottom <= max; bottom+=increment) */
        for (let left = min; left <= max + 20; left += increment)
            /* for (let right = min; right <= max; right+=increment)  */
            nizPozicija.push([top + epsilon * Math.random(), bottom + epsilon * Math.random(), left + epsilon * Math.random(), right + epsilon * Math.random()]);
    /* console.log(nizPozicija); */
    return nizPozicija;
}





























/**
 * Vraca sve css varijable i njigove vrednosti - kao jedan objekat
 * @return {Object} key - nazivi varijabli, value - vred. css varijabli
 */
function getCssVariables_asObj() {
    var declaration = document.styleSheets[0].cssRules[0];
    var allVar = declaration.style.cssText.split(";");

    var result = {};
    for (var i = 0; i < allVar.length; i++) {
        var a = allVar[i].split(':');
        if (a[0] !== "")
            result[a[0].trim()] = a[1].trim();
    }
    /* console.log(result); */
    return result;
}

/**
 * Vraca sve css varijable - samo nazive varijabli - kao niz
 * @return {Array} niz - naziva varijabli
 */
function getCssVariables_asArray() {
    var declaration = document.styleSheets[0].cssRules[0];
    var allVar = declaration.style.cssText.split(";");

    var result = [];
    for (var i = 0; i < allVar.length; i++) {
        var a = allVar[i].split(':');
        if (a[0] !== "")
            result.push(a[0].trim());
    }
    /* console.log(result); */
    return result;
}
